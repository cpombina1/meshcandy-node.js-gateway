$(document).ready(function () {
    var socket = null;
    socket = io();

    var ip = null;
    var ip_gateway = null;
    var mac = null;
    var port = null;
    var conn_type = null;

    var ssid = null;
    var username = null;
    var domainname = null;
    var timezone = null;
    var loggedIn = null;
    var uptime = null;
    var upOk = 0;
    var upErr = 0;
    var downOk = 0;
    var downErr = 0;

    var loading_disp = $('#loading');
    var login_disp = $('#login');
    var monitor_disp = $('#monitor');
    var stat_disp = $('#statistics');
    var home_disp = $('#home');
    var global_disp = $('#global');
    var wifi_disp = $('#wifi');

    var n_account = $('#n_account');
    var n_domain = $('#n_domain');
    var n_uptime = $('#n_uptime');
    var n_method = $('#n_method');
    var n_uploadh = $('#n_uph');
    var n_downloadh = $('#n_downh');
    var n_port = $('#n_port');
    var n_mac = $('#n_mac');
    var n_ap = $('#n_ap');
    var n_ip = $('#n_ip');
    var n_ip_g = $('#n_ip_g');

    var b_conn = $('#b_conn');
    var b_reboot = $('#b_reboot');

    var login_status = $('#login_status td div');
    var login_comment = $('#login_status .comment');
    //"global" status
    var mesh_status = $('#mesh_status td div');
    var mesh_comment = $('#mesh_status .comment');
    
    var wss_status = $('#web_status td div');
    var wss_comment = $('#web_status .comment');
    
    var server_status = $('#server_status td div');
    var server_comment = $('#server_status .comment');


    var wifi_list = $('#wifi_list');
    //LOG IN

    var et_password = $('#password-password');
    var login_button = $('#login_ok');
    var login_cancel = $('#login_cancel');
    var et_server = $('#custom_server');
    var cust_box = $('#custom_url_box');
    var cust_button = $('#cust_button');
    var et_username = $('#username-text');
    var pass = "";
    var passmask;
    var custPressed = false;
    var b_login = $('.login_button');
    var b_mesh = $('#mesh_button');
    var b_web = $('#web_button');
    var isLoggedIn = false;
    var isMonOpen = false;

    var nodes = [];
    var activeNode = null;
    //HEADER NAV

    var nav_home = $('#home_link');
    var nav_stat = $('#stat_link');
    var nav_mon = $('#monitor_link');


    b_reboot.click(function() {
    	var isReboot = confirm("Are you sure you want to reboot?");
    	
    	if(isReboot){
            socket.emit('reboot', true);
    	}
    });

    nav_home.click(function () {
        if (isLoggedIn) {
            showHome();
        }
    });

    nav_stat.click(function () {
        if (isLoggedIn) {
            showStat();
        }
    });

    nav_mon.click(function () {
        if (isLoggedIn) {
            showMon();
        }
    });

    function updateHome() {

        var upHlth = upOk + ":" + upErr;
        var downHlth = downOk + ":" + downErr;
        n_uploadh.text(upHlth);
        n_downloadh.text(downHlth);
        n_account.text(username);
        n_domain.text(domainname);
        n_uptime.text(uptime);
        n_method.text(conn_type);
        n_ap.text(ssid);
        n_ip.text(ip);
        n_ip_g.text(ip_gateway);
        n_port.text(port);
        n_mac.text(mac);
        
    }

    b_conn.click(function () {
        var wifi_pass = $('#wifi_pass');
        var chg_wifi = $('#chg_wifi');
        var canc_wifi = $('#canc_wifi');

        if (socket != null) {
            socket.emit('wifi', true);
        }
        wifi_disp.css('display', 'block');
        login_disp.css('display', 'none');

        chg_wifi.off('click').on('click', function () {
            var sel_wifi = wifi_list.val();
            console.log(sel_wifi);
            var conf = confirm("Are you sure to switch to " + sel_wifi + "?");

            if (conf) {
                if (socket != null) {
                    socket.emit('change_wifi', {ssid: sel_wifi, password: wifi_pass.val()});
                    wifi_disp.css('display', 'none');
                }
            }
        });

        canc_wifi.click(function () {
            wifi_disp.css('display', 'none');
        });




    });

    function showHome() {
        nav_home.addClass('active');
        nav_stat.removeClass('active');
        nav_mon.removeClass('active');
        loading_disp.css('display', 'none');
        global_disp.css('display', 'block');
        login_disp.css('display', 'none');
        home_disp.css('display', 'block');
        monitor_disp.css('display', 'none');
        stat_disp.css('display', 'none');
    }
    
    b_login.off('click').on('click', function () {
    	login_disp.css('display', 'block');
    });

    function showLogin() {
            isLoggedIn = false;
            b_login.html("Log In");
            login_comment.html("Logged out");
            login_status.css('background-color', '#ff0000');
            cust_button.off('click').on('click', function () {
                et_server.val("");
                if (custPressed) {
                    cust_box.css('display', 'none');
                    cust_button.html("Click here if custom server");
                    custPressed = false;
                } else {
                    cust_box.css('display', 'block');
                    cust_button.html("Cancel");
                    custPressed = true;
                }
            });
            $('#username-clear').show();
            $('#username-text').hide();

            $('#username-clear').focus(function() {
                $('#username-clear').hide();
                $('#username-text').show();
                $('#username-text').focus();
            });
            $('#username-text').blur(function() {
                if($('#username-text').val() == '') {
                    $('#username-clear').show();
                    $('#username-text').hide();
                }
            });
            
            $('#password-clear').show();
            et_password.hide();

            $('#password-clear').focus(function() {
                $('#password-clear').hide();
                et_password.show();
                et_password.focus();
            });
            et_password.blur(function() {
                if(et_password.val() == '') {
                    $('#password-clear').show();
                    et_password.hide();
                }
            });
	    
	    login_cancel.off('click').on('click', function () {
                    et_username.val("");
                    et_password.val("");
                    et_server.val("");
                    pass = "";
                    et_server = "";
        	    login_disp.css('display', 'none');
      
	    });
            
            login_button.click(function () {
                if (et_username.val().length !== 0 && et_password.val().length !== 0) {
                    console.log({username: et_username.val(), password: et_password.val()});
                    socket.emit('login', {login: true, username: et_username.val(), password: et_password.val(), serverurl: et_server.val()});
                    et_username.val("");
                    et_password.val("");
                    et_server.val("");
                    pass = "";
                } else {
                    alert("Please enter a username and password");
                }
            });
        login_disp.css('display', 'block');
    }

    function hideLogin() {
            isLoggedIn = true;
            b_login.html("Log Out");
            login_comment.html("OK");
            login_status.css('background-color', '#00ff00');
            if (username !== "") {
                n_account.html(username);
                login_comment.html("Logged in as " + username);
            }

            b_login.off('click').on('click', function () {
                var logout = confirm("Are you sure to log out?");

                if (logout) {
                    socket.emit('login', {login: false});
                }
            });
        login_disp.css('display', 'none');

    }
    
    function showStat() {
        nav_home.removeClass('active');
        nav_stat.addClass('active');
        nav_mon.removeClass('active');
        loading_disp.css('display', 'none');
        global_disp.css('display', 'block');
        login_disp.css('display', 'none');
        home_disp.css('display', 'none');
        monitor_disp.css('display', 'none');
        stat_disp.css('display', 'block');

    }

    function showMon() {
        nav_home.removeClass('active');
        nav_stat.removeClass('active');
        nav_mon.addClass('active');
        loading_disp.css('display', 'none');
        global_disp.css('display', 'block');
        login_disp.css('display', 'none');
        home_disp.css('display', 'none');
        monitor_disp.css('display', 'block');
        stat_disp.css('display', 'none');


        var cmd_log = $('#cmd_log');
        var mon_list = $('#mon_list');
        var cmdIdType = $('#cmdIdType');

        if (!isMonOpen) {
            isMonOpen = true;
            socket.on('info', function (res) {
                cmd_log.append($('<li>').text(res));
            });

            socket.on('nodes', function (obj) {
                console.log(nodes);
                var newNodes = Object.keys(obj);
                console.log(newNodes);
                for (var i = 0; i < newNodes.length; i++) {
                    if (nodes.indexOf(newNodes[i]) < 0) {
                        nodes.push(newNodes[i]);
                        var nodeName = newNodes[i];
                        mon_list.append($('<li>').html('<span class="node" id="' + newNodes[i] + '">' + newNodes[i]));
                        var cNode = $('#' + newNodes[i]);
                        $(cNode).click(function () {
                            console.log("selected " + newNodes[i]);
                            if (activeNode !== null) {
                                activeNode.removeClass('node_en');
                            } else {
                                activeNode = this;
                                this.addClass('node_en');
                            }
                        });
                    }
                }
            });

        }

    }

    function checkLogin(isLogin) {


        if (isLogin) {
	    hideLogin();
        } else {
            showLogin();
        }
    }

    //Check USB Connection

    b_mesh.click(function () {
        socket.emit('mesh', {reset: true});
    });
    socket.on('isconnect', function (res) {
        var attached = res['attached'];
        var usb_connect = res['usb_connect'];
        var comment = res['comment'];

        if (attached) {
            if (usb_connect) {
                mesh_status.css('background-color', '#00ff00');
                if (comment !== null)
                    mesh_comment.html(comment);
            } else {
                mesh_status.css('background-color', '#ffff00');
                if (comment !== null)
                    mesh_comment.html(comment);
            }
        } else {
            mesh_status.css('background-color', '#ff0000');
            if (comment !== null)
                mesh_comment.html(comment);
        }

    });
    socket.on('disconnect', function (result) {
        console.log("disconnected from server");
        wss_status.css('background-color', '#ff0000');
        wss_comment.html("Disconnect from interface.");
        //var login = confirm("You have been disconnected, reconnect?");

        //if (login) {
        //   socket = io();
        //}

    });

    b_web.click(function () {
        socket.disconnect();
    });

    socket.on('home', function (res) {
        console.log(res);
        if (res['error'] == null) {

            ip = res['ip'];
            ip_gateway = res['ip_gateway'];
            mac = res['mac'];
            port = res['port'];
            conn_type = res['type'];
            wifi = res['wifi'] === "connected" ? true : false;
            ssid = res['ssid'];
            updateHome();
        } else {
            alert(res['error']);
        }

    });
    socket.on('wifi', function (res) {
        wifi_list.empty();
        var option = '';
        for (var i = 0; i < res.length; i++) {
            option += '<option value="' + res[i] + '">' + res[i] + '</option>';
        }
        wifi_list.append(option);
    });
    socket.on('change_wifi', function (res) {
        alert(res);

    });

    socket.on('connect', function (res) {
    	loading_disp.css('display', 'none');
        home_disp.css('display', 'block');
        global_disp.css('display', 'block');
        wss_status.css('background-color', '#00ff00');
        wss_comment.html("OK");
        console.log("connected to server");

    });
    socket.on('login', function (res) {
        if (res['error'] == null) {
            console.log(res);
            username = res['username'];
            loggedIn = res['login'];
            domainname = res['domainname'];
            uptime = res['uptime'];
            upOk = res['upOk'];
            upErr = res['upErr'];
            downOk = res['downOk'];
            downErr = res['downErr'];
            
            var tickOk = upOk + downOk;
            var tickErr = upErr + downErr;
            var tickAll = tickOk + tickErr;
            
            if(tickOk / tickAll > 0.90){
                console.log("health ok");
                server_status.css('background-color', '#00ff00');
                server_comment.html("OK");
            }else if(tickOk / tickAll < 0.90 && tickOk / tickAll > 0.75){
                console.log("health warning");
                server_status.css('background-color', '#ffff00');
                server_comment.html("Warning");
            }else if(tickOk / tickAll < 0.75){
                console.log("health bad");
                server_status.css('background-color', '#ff0000');
                server_comment.html("Please contact support");
            }
            checkLogin(res['login']);
        } else {
            alert(res['error']);
        }
    });

    function repeat(s, n) {
        return new Array(isNaN(n) ? 1 : ++n).join(s);
    }
});

