/*
 * MeshCandy Gateway
 * --- ToC ---
 * Start up
 * JSON Writer
 * Express
 * Wifi Control & Network
 * Parse Server Functions
 * Send to Serial
 * Scheduler
 * Socket.io
 * USB
 * Utils
 * Log Handling
 */
 
 // MAKE TRUE TO CREATE LOG
 var enableLogging = false;
 
//IMPORT REQUIREMENTS
var http = require('http');
var express = require('express');
var path = require('path');
var serialport = require('serialport');
var schedule = require('node-schedule');
var moment = require('moment');
var usb = require('usb');
var jsonfile = require('jsonfile');
var os = require('os');
var WiFiControl = require('wifi-control');
var network = require('network');
//Lets define a port we want to listen to
var LOCALPORT = 808;
var PORT = 80;	//PROD
//var PORT = 8081;	//DEV
//var SERVER_URL = 'https://api.parse.com';
var SERVER_URL = 'http://candybowl.meshcandy.com';
//var SERVER_URL = 'localhost';
//var SERVER_URL = 'http://209.64.98.135:1337';
var APPLICATION_ID = "myAppId";
var REST_KEY = "myRestKey";
//var APPLICATION_ID = "1h7oWiiVAGRtwceikvYS68WDi40QB9RVrowYEk40";
//var REST_KEY = "CprIYKkVlDYjKwMOybh7bmT4AeP4qAttwojAUXsx";
var USERNAME = null;
var PASSWORD = null;
var SESSION_TOKEN = "";
var SESSION_ID = "";
var INSTALLATION_ID = "";
var USER_ID = "";
var LOGIN_CRED = "";
var DOMAIN_NAME = "";
var DOMAIN_TZONE = "";
var USB_PORT_NAME = null;
var configFile = './config.json';
var logFile = './logFile.log';
var statLog = './statLog.json';

//For statistics
var upOk = 0;
var upErr = 0;
var downOk = 0;
var downErr = 0;

var identSubs = ["eValue", "batteryValue"];
var socket;
var sPort;
var usbStatus = "";
var isLoggedIn = false;
var isUsbAttached = false;
var isCommActive = false;
var isSocketOn = false;
var startTime = moment();
var osEnv = os.type();
var osPlatform = os.platform();
var osArch = os.arch();
var osHostname = os.hostname();
var osRelease = os.release();
var isEnoent = false;

var osInfo = {env: osEnv, platform: osPlatform, arch: osArch, hostname: osHostname, release: osRelease};
toLog(JSON.stringify(osInfo), INFO);
var nodeObj = {};

/***********************************************

Testing

 *********************************************/
 /*

 var testCt = 0;
var the_interval = 20 * 1000;
 setInterval(function() {
   if(isLoggedIn) {
	   //sendData("0000", "batteryValue", "19"); 
	   //sendData("clyde", "buttonData", "1"); 
	   for(var i = 0; i < 2; i++){
		sendData("dev" + i, "eValue", "[211" + testCt + ",0,0]");
		testCt++;
	   }
	   
		//var time = moment();
		//var logPhrase = moment().format("MM/DD/YY HH:mm0HH.mm");
        // emSetValue("KM50001","msgData", "\"" + logPhrase + "I\"");
		 if(testCt = 10) testCt = 0;
         //emSetValue("KM50001","msgData","\"02/16/17 10:15050.10I\"");
   }
 }, the_interval);
 */
/////////////////////////////////////////////////
/***********************************************

 START UP

 *********************************************/


toLog("MeshCandy Gateway started since " + startTime.format("MM/DD/YYYY, hh:mm:ss a"), INFO);
getConfig();
//login();           //Log in to Parse server
getPort();         //Try to find port on start up
/////////////////////////////////////////////////
/***********************************************

Bluetooth

 *********************************************/
 /*
 var bleno = require('bleno');
 var util = require('util');

 var BlenoPrimaryService = bleno.PrimaryService;
var BlenoCharacteristic = bleno.Characteristic;

console.log('bleno - echo');

bleno.on('stateChange', function(state) {
  console.log('on -> stateChange: ' + state);

  if (state === 'poweredOn') {
    bleno.startAdvertising('MeshCandy Gateway', ['fff0']);
  } else {
    bleno.stopAdvertising();
  }
});

var buff = new Buffer("hello");
bleno.on('advertisingStart', function(error) {
  console.log('on -> advertisingStart: ' + (error ? 'error ' + error : 'success'));
if(error){
	
		bleno.stopAdvertising(function(error) {
			
				toLog("BLE Stopping", INFO);
		});
}
  if (!error) {
	//setTimeout(function() {
	//	bleno.stopAdvertising(function(error) {
			
	//			toLog("BLE Stopping", INFO);
	//	});
	//}, 10000);
	
    bleno.setServices([
      new BlenoPrimaryService({
        uuid: 'fff0',
        characteristics: [new GetWifiCharacteristics(),new SetWifiCharacteristics(),new GetIpAddrChar(),new GetIpGwAddrChar ()
        ]
      }),
      new BlenoPrimaryService({
        uuid: 'ffe0',
        characteristics: [new GetCommChar(), new GetLoginChar(), new GetUsbChar(), new SetPassChar(), new SetUserChar(), new SetUrlChar()
        ]
      })
    ]);
  }
});
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// GET WIFI Characteristics
var GetWifiCharacteristics = function() {
		  GetWifiCharacteristics.super_.call(this, {
			uuid: 'fff1',
			properties: ['notify'],
			value: null
		  });

};

util.inherits(GetWifiCharacteristics, BlenoCharacteristic);

GetWifiCharacteristics.prototype.onSubscribe = function(maxValueSize, updateValueCallback) {
  console.log('GetWifiCharacteristics - onSubscribe');

        toLog("Scan WiFi");
        //  Try scanning for access points:
        WiFiControl.scanForWiFi(function (err, response) {
			console.log(response);
			networks = [];
		var netStr = "";
            if (err) {
					callback(self.RESULT_SUCCESS, new Buffer("error"));
            } else {
                for (var i = 0; i < response['networks'].length; i++){
                    networks.push(response['networks'][i]['ssid']);
					if(i == response['networks'].length - 1){
						var count = 0;
						var netInterval = setInterval(function() {
									console.log('GetWifiCharacteristics - onWriteRequest: notifying ' +  (count + 1) + "/" + networks.length);

									updateValueCallback(new Buffer(networks[count].substring(0,12) + "..."));
									count++;
									if(count >= networks.length) clearInterval(netInterval);
						},1000);
						
  
					}
			}
				}
            });
};

GetWifiCharacteristics.prototype.onUnsubscribe = function() {
  console.log('GetWifiCharacteristics - onUnsubscribe');

};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// SET WIFI Characteristics
var SetWifiCharacteristics = function() {
		  SetWifiCharacteristics.super_.call(this, {
			uuid: 'fff2',
			properties: ['write', 'notify'],
			value: null
		  });

		  this._value = new Buffer(0);
		  this._updateValueCallback = null;
};

SetWifiCharacteristics.prototype.onWriteRequest = function(data, offset, withoutResponse, callback) {

  var arr = (data.toString()).split("|");
  console.log(arr);
  var ap = {
    ssid: networks[arr[0]],
    password: arr[1]
  };
  console.log(ap);
        var results = WiFiControl.connectToAP(ap, function (err, response) {
            if (err) {
				  if (this._updateValueCallback) {
					console.log('SetWifiCharacteristics - onWriteRequest: notifying');

					this._updateValueCallback(new Buffer("failure"));
				  }
                toLog(err, ERROR);
                console.log(err);
            } else {
				  if (this._updateValueCallback) {
					console.log('SetWifiCharacteristics - onWriteRequest: notifying');

					this._updateValueCallback(new Buffer("success"));
				  }
                toLog("Network Changed", INFO);
                updateIp();
            }
        });
  console.log('SetWifiCharacteristics - onWriteRequest: value = ' + data.toString());

  callback(this.RESULT_SUCCESS);
};

SetWifiCharacteristics.prototype.onSubscribe = function(maxValueSize, updateValueCallback) {
  console.log('SetWifiCharacteristics - onSubscribe');

  this._updateValueCallback = updateValueCallback;
};

SetWifiCharacteristics.prototype.onUnsubscribe = function() {
  console.log('SetWifiCharacteristics - onUnsubscribe');

  this._updateValueCallback = null;
};

util.inherits(SetWifiCharacteristics, BlenoCharacteristic);


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// GET IP Characteristics
var GetIpAddrChar = function() {
		  GetIpAddrChar.super_.call(this, {
			uuid: 'fff3',
			properties: ['read'],
			value: null
		  });

};

util.inherits(GetIpAddrChar, BlenoCharacteristic);

GetIpAddrChar.prototype.onReadRequest = function(offset, callback) {
  //console.log('GetIpAddrChar - onReadRequest: value = ' + this._value.toString('hex'));
  callback(this.RESULT_SUCCESS, new Buffer(ip));
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// GET IP Gateway Characteristics
var GetIpGwAddrChar = function() {
		  GetIpGwAddrChar.super_.call(this, {
			uuid: 'fff4',
			properties: ['read'],
			value: null
		  });

};

util.inherits(GetIpGwAddrChar, BlenoCharacteristic);

GetIpGwAddrChar.prototype.onReadRequest = function(offset, callback) {
  //console.log('GetIpGwAddrChar - onReadRequest: value = ' + this._value.toString('hex'));
  callback(this.RESULT_SUCCESS, new Buffer(ip_gateway));
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// GetLoginChar Characteristics
var GetLoginChar = function() {
		  GetLoginChar.super_.call(this, {
			uuid: 'ffe1',
			properties: ['read'],
			value: null
		  });

};

util.inherits(GetLoginChar, BlenoCharacteristic);
GetLoginChar.prototype.onReadRequest = function(offset, callback) {
  //console.log('GetLoginChar - onReadRequest: value = ' + this._value.toString('hex'));
  callback(this.RESULT_SUCCESS, new Buffer(isLoggedIn ? "1" : "0"));
};
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// GetUsbChar Characteristics
var GetUsbChar = function() {
		  GetUsbChar.super_.call(this, {
			uuid: 'ffe2',
			properties: ['read'],
			value: null
		  });

};

util.inherits(GetUsbChar, BlenoCharacteristic);
GetUsbChar.prototype.onReadRequest = function(offset, callback) {
  //console.log('GetUsbChar - onReadRequest: value = ' + this._value.toString('hex'));
  callback(this.RESULT_SUCCESS, new Buffer(isUsbAttached ? "1" : "0"));
};
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// GetCommChar Characteristics
var GetCommChar = function() {
		  GetCommChar.super_.call(this, {
			uuid: 'ffe3',
			properties: ['read'],
			value: null
		  });

};

util.inherits(GetCommChar, BlenoCharacteristic);
GetCommChar.prototype.onReadRequest = function(offset, callback) {
  //console.log('GetCommChar - onReadRequest: value = ' + this._value.toString('hex'));
  callback(this.RESULT_SUCCESS, new Buffer(isCommActive ? "1" : "0"));
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// SetUserChar Characteristics
var SetUserChar = function() {
		  SetUserChar.super_.call(this, {
			uuid: 'ffe4',
			properties: ['write'],
			value: null
		  });

};

util.inherits(SetUserChar, BlenoCharacteristic);

var userToSet = null;
var passToSet = null;
SetUserChar.prototype.onWriteRequest = function(data, offset, withoutResponse, callback) {

  userToSet = data.toString();

if (loginCallback) {
    console.log('EchoCharacteristic - onWriteRequest: notifying');

	if(userToSet && passToSet) loginCallback(new Buffer("Logging in"));
  }
  callback(this.RESULT_SUCCESS);
};


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// SetPassChar Characteristics
var SetPassChar = function() {
		  SetPassChar.super_.call(this, {
			uuid: 'ffe5',
			properties: ['write', 'notify'],
			value: null
		  });

};

util.inherits(SetPassChar, BlenoCharacteristic);

var passToSet = null;
var loginCallback = null;

SetPassChar.prototype.onWriteRequest = function(data, offset, withoutResponse, callback) {

  passToSet = data.toString();
  if(userToSet && passToSet){
	  if(isLoggedIn) logout();
	  login(userToSet, passToSet, urlToSet);
  }
if (loginCallback) {
    console.log('SetPassChar - onWriteRequest: notifying');

	if(userToSet && passToSet) loginCallback(new Buffer("Logging in"));
  }
  
  callback(this.RESULT_SUCCESS);
};

SetPassChar.prototype.onSubscribe = function(maxValueSize, updateValueCallback) {
  console.log('SetPassChar - onSubscribe');

  loginCallback = updateValueCallback;
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// SetUrlChar Characteristics
var SetUrlChar = function() {
		  SetUrlChar.super_.call(this, {
			uuid: 'ffe6',
			properties: ['write'],
			value: null
		  });

};

util.inherits(SetUrlChar, BlenoCharacteristic);

var urlToSet = "";
SetUrlChar.prototype.onWriteRequest = function(data, offset, withoutResponse, callback) {

  urlToSet = data.toString();

if (loginCallback) {
    console.log('EchoCharacteristic - onWriteRequest: notifying');

	if(userToSet && passToSet) loginCallback(new Buffer("Logging in"));
  }
  callback(this.RESULT_SUCCESS);
};
*/
/***********************************************

 JSON writer	

 *********************************************/
function getConfig() {
    jsonfile.readFile(configFile, function (err, obj) {
        if (obj != null) {
            //var jsonObj =  JSON.parse(obj);
            USERNAME = obj['username'];
            PASSWORD = obj['password'];

            //if (obj['installation_id'] != null)
            //    INSTALLATION_ID = obj['installation_id'];
            if (obj['serverurl'] != null)
                SERVER_URL = obj['serverurl'];
            login(USERNAME, PASSWORD, SERVER_URL);
        } else {
            toLog("Log does not exist", INFO);
        }
    });
}
function updateConfig(obj) {
    var jsonfile = require('jsonfile');

    jsonfile.writeFile(configFile, obj, {spaces: 2}, function (err) {
        if (err != null)
            toLog(err, ERROR);
    });
}
/////////////////////////////////////////////////
/***********************************************

 File writer

 *********************************************/
function createHTMLShortcut(address) {


    //Using str.replace  (global) creates weird results...
    var shortcutPage = '<!DOCTYPE HTML><html lang="en-US"><head><meta charset="UTF-8"><meta http-equiv="refresh" content="' + address +
            '"><script type="text/javascript">window.location.href = "' + address +
            '"</script><title>Page Redirect</title></head><body>If you are not redirected automatically, follow the <a href="' + address + '">link to the MeshCandy Gateway Portal</a></body></html>';

    fs.writeFile('./gateway/Gateway Portal.html', shortcutPage, 'utf8', function (err) {
        if(err == null) toLog("Created Gateway shortcut to " + address);
    });
}

function appendToLog(text) {

    var time = moment();
    var fileName = "MCGWLOG" + moment().format("MMDDYY");
	
	var fs = require('fs');
    fs.appendFile('./log/' + fileName + '.txt', text + "\r\n", 'utf8', function (err) {
        if(err != null) toLog("Error creating log!", ERROR);
    });
}
/////////////////////////////////////////////////
/***********************************************

 EXPRESS

 *********************************************/
var express = require('express'),
        app = express(),
        http = require('http').createServer(app).listen(LOCALPORT, function () {
    toLog('webserver running on port ' + LOCALPORT + '.', INFO);
}),
        sys = require('util'),
        exec = require('child_process').exec,
        io = require('socket.io')(http),
        fs = require('fs');

app.use('/assets', express.static('assets'));

app.get('/who', function (req, res) {
  res.send('MeshCandy Server');
});

app.get("/", function (req, res) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    fs.readFile('./index.html', 'utf8', function (err, data) {
        res.end(data);
    });
});
/////////////////////////////////////////////////
/***********************************************

 WIFI Control & network

 *********************************************/
var WiFiControl = require('wifi-control');

//  Initialize wifi-control package with verbose output
WiFiControl.init({
    debug: true
});

var ifaceState = WiFiControl.getIfaceState();
console.log(ifaceState);
var wifiConnected = ifaceState['connection'] + "";
var connection_type = null;
var ssid = ifaceState['ssid'] + "";
var ip = null;
var ip_gateway = null;
var mac = null;

function updateIp() {
    network.get_active_interface(function (err, obj) {
        if (err != null) {
            toLog(err, ERROR);
        } else {
            ifaceState = WiFiControl.getIfaceState();
            ssid = ifaceState['ssid'] + "";
            ip = obj['ip_address'];
            mac = obj['mac_address'];
            ip_gateway = obj['gateway_ip'];
            connection_type = obj['name'];
            console.log(ifaceState);
            console.log(obj);
            createHTMLShortcut("http://" + ip);
            if (socket != null) {

                socket.emit('home', {ip: ip, ip_gateway: ip_gateway, mac: mac, port: PORT, type: connection_type, ssid: ssid});

            }
        }
        /*

         { power: true,
         connection: 'connected',
         ssid: 'TRENDnet',
         success: true,
         msg: 'Successfully acquired state of network interface wlan0.' }
         { name: 'wlan0',
         ip_address: '192.168.1.77',
         mac_address: '74:da:38:0d:4c:1f',
         gateway_ip: '192.168.1.1\n192.168.1.1',
         netmask: 'Mask:255.255.255.0' }

         */
    });
}
updateIp();
/////////////////////////////////////////////////
/***********************************************

 PARSE Fx

 *********************************************/
function login(username, pass, serverurl) {

    if (username != null && pass != null) {

        toLog(serverurl, DEBUG);
         if (serverurl.length != 0) {
         if (serverurl.indexOf("http") > -1) {
         SERVER_URL = serverurl;
         } else {
         SERVER_URL = "http://" + serverurl;
         }
         }else{
			serverurl = SERVER_URL;
		 }

        toLog(SERVER_URL, DEBUG);
        var querystring = require('querystring');
        var postData = querystring.stringify({
            username: username,
            password: pass
        });
		var reqHttp;
		var urlPath;
		if (serverurl.indexOf("https") > -1) {
			reqHttp = require('https');
			urlPath = serverurl.substring(8);
			console.log("HTTPS");
		} else if (serverurl.indexOf("localhost") > -1){
			reqHttp = require('http');
			urlPath = 'localhost';
			console.log("HTTP");
        } else {
			reqHttp = require('http');
			urlPath = serverurl.substring(7);
			console.log("HTTP");
        }
		console.log(urlPath);

		var path = '';
		if(SERVER_URL.indexOf("api.parse") > -1){
			path = '/1/login';
		}else{
			path = '/parse/login?%s';
		}

		console.log(path);
        var options = {
            host: urlPath,
			port: PORT,
            path: path,
			rejectUnauthorized: false,
			form: {username: username, password: pass},
            method: 'GET',
            headers: {
                "X-Parse-Application-Id": APPLICATION_ID,
                "X-Parse-REST-API-Key": REST_KEY,
                "X-Parse-Revocable-Session": "1",
                "Content-Type": 'application/x-www-form-urlencoded',
                //"Content-Type": "application/json",
                'Content-Length': postData.length
            }
        };

//{"ACL":{"HZ6cpQseNi":{"read":true,"write":true}},
//"createdAt":"2014-09-16T21:20:51.611Z",
//"domain":{"__type":"Pointer","className":"_Role","objectId":"b81dyeyTtV"},
//"email":"",
//"emailVerified":false,
//"firstname":"User",
//"lastname":"Demo",
//"objectId":"HZ6cpQseNi",
//"profile":{"__type":"File","name":"tfss-e4e41d60-3a1f-42e4-8c1f-0443aaf85cc5-thumbnail.jpg",
//"url":"http://files.parsetfss.com/e8df2409-f4f3-41ae-9a55-0d93ac5c1f7f/tfss-e4e41d60-3a1f-42e4-8c1f-0443aaf85cc5-thumbnail.jpg"},
//"sessionToken":"r:AGsJmcxWbNn8mVvKaKRL7tnYZ",
//"updatedAt":"2015-12-29T18:16:08.436Z",
//"username":"test"}

        var req = reqHttp.request(options, function (res) {
            var body = '';
            res.on('data', function (chunk) {
                body += chunk;
            });
            res.on('end', function () {
				try{
					var jsonObj = JSON.parse(body);
					console.log(jsonObj);
				}catch (err) {
					toLog("Communication error", ERROR);
                    upErr++;
                    toLog("Login error: ", ERROR);
                    isLoggedIn = false;
                    if (socket != null) {
                        socket.emit('login', {login: isLoggedIn, username: USERNAME, error: "Communication Error",
                                                    uptime: startTime.format("MM/DD/YYYY, hh:mm:ss a"), upOk: upOk, upErr: upErr, downOk: downOk, downErr: downErr});
                    }
					return;
				}
                
                if (jsonObj['error'] != null) {
                    upErr++;
                    toLog("Login error: ", ERROR);
                    isLoggedIn = false;
                    if (socket != null) {
                        socket.emit('login', {login: isLoggedIn, username: USERNAME, error: "Communication Error",
                                                    uptime: startTime.format("MM/DD/YYYY, hh:mm:ss a"), upOk: upOk, upErr: upErr, downOk: downOk, downErr: downErr});
                    }
                } else {
                    upOk++;
                    var domainId = jsonObj['domain']['objectId'];
                    SESSION_TOKEN = jsonObj['sessionToken'];
                    USERNAME = jsonObj['username'];
                    console.log(USERNAME);
                    USER_ID = jsonObj['objectId'];
                    toLog(domainId, DEBUG);
                    toLog(SESSION_TOKEN, DEBUG);
                    //console.log(response.statusCode, body);
                    isLoggedIn = true;

                    var nodeUuid = require('node-uuid');
                    var uuid = nodeUuid.v4();

					var path = '';
					if(SERVER_URL.indexOf("api.parse") > -1){
						path = '/1/sessions';
					}else{
						path = '/parse/sessions';
					}
                    //var https1 = require('https');
                    var options1 = {
                        host: urlPath,
                        path: path,
						port: PORT,
                        method: 'POST',
						rejectUnauthorized: false,
                        headers: {
                            "X-Parse-Application-Id": APPLICATION_ID,
                            "X-Parse-REST-API-Key": REST_KEY,
                            "X-Parse-Session-Token": SESSION_TOKEN
                        }
                    };

                    var req1 = reqHttp.request(options1, function (res) {
                        var body1 = '';
                        res.on('data', function (chunk) {
                            body1 += chunk;
                        });
                        res.on('end', function () {

                            console.log(body1);
							
							try{
								var jsonObj = JSON.parse(body1);
								console.log(jsonObj);
							}catch (err) {
								toLog("Communication error", ERROR);
								upErr++;
								toLog("Login error: ", ERROR);
								isLoggedIn = false;
								if (socket != null) {
									socket.emit('login', {login: isLoggedIn, username: USERNAME, error: jsonObj["error"],
																uptime: startTime.format("MM/DD/YYYY, hh:mm:ss a"), upOk: upOk, upErr: upErr, downOk: downOk, downErr: downErr});
								}
								return;
							}
							
							
                            SESSION_TOKEN = jsonObj['sessionToken'];

                            var nodeUuid = require('node-uuid');
                            var uuid = nodeUuid.v4();

							var path = '';
							if(SERVER_URL.indexOf("api.parse") > -1){
								path = '/1/sessions/me';
							}else{
								path = '/parse/sessions/me';
							}

                            var options2 = {
                                host: urlPath,
                                path: path,
								port: PORT,
                                method: 'PUT',
								rejectUnauthorized: false,
                                headers: {
                                    "X-Parse-Application-Id": APPLICATION_ID,
                                    "X-Parse-REST-API-Key": REST_KEY,
                                    "X-Parse-Session-Token": SESSION_TOKEN,
                                    "X-Parse-Installation-Id": uuid
                                }
                            };

                            var req2 = reqHttp.request(options2, function (res) {
                                var body2 = '';
                                res.on('data', function (chunk) {
                                    body2 += chunk;
                                });
                                res.on('end', function () {
                                    console.log(jsonObj);
                                    SESSION_ID = jsonObj['objectId'];
                                    INSTALLATION_ID = uuid;
                                    upOk++;
                                    var toSave = {username: USERNAME, password: pass, user_id: USER_ID, serverurl: SERVER_URL, session_token: jsonObj['sessionToken'], installation_id: uuid};
                                    updateConfig(toSave);
                                    toLog("Logged in", INFO);

                                    var querystring = require('querystring');

                                    var postData3 = querystring.stringify({
                                        where: '{"objectId": "' + domainId + '"}',
                                        "include": "domain"
                                    });

									var path = '';
									if(SERVER_URL.indexOf("api.parse") > -1){
										path = '/1/roles?';
									}else{
										path = '/parse/roles?';
									}

                                    var options3 = {
                                        host: urlPath,
                                        path: path + postData3,
                                        method: 'GET',
										port: PORT,
										rejectUnauthorized: false,
                                        headers: {
                                            "X-Parse-Application-Id": APPLICATION_ID,
                                            "X-Parse-REST-API-Key": REST_KEY,
                                            "X-Parse-Session-Token": SESSION_TOKEN
                                        }
                                    };

                                    var req3 = reqHttp.request(options3, function (res) {
                                        var body3 = '';
                                        res.on('data', function (chunk) {
                                            body3 += chunk;
                                        });
                                        res.on('end', function () {
                                            console.log("Got Domain Info");
											
											try{
												var jsonObj = JSON.parse(body3);
												console.log(jsonObj);
											}catch (err) {
												toLog("Communication error", ERROR);
												upErr++;
												toLog("Login error: ", ERROR);
												isLoggedIn = false;
												if (socket != null) {
													socket.emit('login', {login: isLoggedIn, username: USERNAME, error: jsonObj["error"],
																				uptime: startTime.format("MM/DD/YYYY, hh:mm:ss a"), upOk: upOk, upErr: upErr, downOk: downOk, downErr: downErr});
												}
												return;
											}
                                            var domainObj = JSON.parse(body3);
                                            DOMAIN_NAME = domainObj['results'][0]['name'];
                                            DOMAIN_TZONE = domainObj['results'][0]['timeZone'];
                                            console.log(DOMAIN_NAME);
                                            console.log(DOMAIN_TZONE);
                                            upOk++;
                                            if (socket != null) {
                                                socket.emit('login', {login: isLoggedIn,
                                                    username: USERNAME,
                                                    serverurl: SERVER_URL,
                                                    domainname: DOMAIN_NAME,
                                                    domaintzone: DOMAIN_TZONE,
                                                    uptime: startTime.format("MM/DD/YYYY, hh:mm:ss a"), upOk: upOk, upErr: upErr, downOk: downOk, downErr: downErr});
                                            }
                                            console.log(domainObj['results'][0]['domain']);
                                            /*

                                             { results:
                                             [ { ACL: [Object],
                                             channel: [Object],
                                             createdAt: '2014-08-27T07:02:11.505Z',
                                             dailySummary: true,
                                             domain: [Object],
                                             name: 'TENACORE',
                                             objectId: 'b81dyeyTtV',
                                             roles: [Object],
                                             timeZone: 'America/Los_Angeles',
                                             updatedAt: '2016-09-20T19:05:31.962Z',
                                             users: [Object] } ] }

                                             */
                                        });
                                        res.on('error', function (err) {
                                            upErr++;
                                            var jsonErr = JSON.parse(err);
                                            var errCode = jsonErr['error']['code'];
                                            console.log(errCode);
                                        });
                                    });

                                    // req error
                                    req3.on('error', function (err) {
                                        upErr++;
                                        var jsonErr = JSON.parse(err);
                                        var errCode = jsonErr['error']['code'];
                                        console.log(errCode);
                                    });
                                    //req3.write(postData3);
                                    req3.end();



                                });
                                res.on('error', function (err) {
                                    upErr++;
                                    var jsonErr = JSON.parse(err);
                                    var errCode = jsonErr['error']['code'];
                                    console.log(errCode);

                                });
                            });

                            // req error
                            req2.on('error', function (err) {
                                upErr++;
                                var jsonErr = JSON.parse(err);
                                var errCode = jsonErr['error']['code'];
                                console.log(errCode);
                            });
                            req2.write("{}");
                            req2.end();
                        });
                        res.on('error', function (err) {
                            upErr++;
                            var jsonErr = JSON.parse(err);
                            var errCode = jsonErr['error']['code'];
                            console.log(errCode);

                        });
                    });

                    // req error
                    req1.on('error', function (err) {
                        upErr++;
                        console.log(err);
                    });
                    req1.write("{}");
                    req1.end();


                }
            });
            res.on('error', function (err) {
                upErr++;
                var jsonErr = JSON.parse(err);
                var errCode = jsonErr['error']['code'];
                console.log(errCode);

            });
        });

        // req error
        req.on('error', function (err) {
            upErr++;
            console.log("Login error" + err);
            if((err + "").includes("ENOENT")){
                isEnoent = true;
                toLog("No internet connection", ERROR);
            }
        });
        req.write(postData);
        req.end();
        /*
         var request = require('request');
         var pathdir = '/1';
         request({
         method: 'GET',
         url: SERVER_URL + pathdir + '/login?%s',
         form: {username: username, password: pass},
         agentOptions: {
         secureProtocol: 'SSLv3_method'
         },
         headers: {
         "X-Parse-Application-Id": APPLICATION_ID,
         "X-Parse-REST-API-Key": REST_KEY,
         "X-Parse-Revocable-Session": "1",
         "Content-Type": "application/json"
         }
         }, function (error, response, body) {

         console.log(body);
         try {
         var jsonObj = JSON.parse(body);
         if (jsonObj['error'] != null) {
         toLog("Login error: ", ERROR);
         isLoggedIn = false;
         if (socket != null) {
         socket.emit('login', {login: isLoggedIn, username: username, error: jsonObj["error"]});
         }
         } else {
         SESSION_TOKEN = jsonObj['sessionToken'];
         USER_ID = jsonObj['objectId'];
         var toSave = {username: username, password: pass, user_id: jsonObj['objectId'], serverurl: SERVER_URL, session_token: jsonObj['sessionToken']};
         updateConfig(toSave);
         toLog("Logged in", INFO);
         //console.log(response.statusCode, body);
         isLoggedIn = true;
         if (socket != null) {
         socket.emit('login', {login: isLoggedIn, username: username, serverurl: SERVER_URL});
         }
         }

         } catch (err) {
         toLog("Login error: Server not responding", ERROR);
         toLog(error, ERROR);
         toLog(response, ERROR);
         toLog(body, ERROR);
         isLoggedIn = false;
         if (socket != null) {
         socket.emit('login', {login: isLoggedIn, username: username, error: "Server not responding"});
         }
         }
         });
         */
    } else {

    }
}

var logout = function () {
    if (isLoggedIn) {
		var reqHttp;
		var urlPath;
		if (SERVER_URL.indexOf("https") > -1) {
			reqHttp = require('https');
			urlPath = SERVER_URL.substring(8);
		} else {
			reqHttp = require('http');
			urlPath = SERVER_URL.substring(7);
        }
		var path = '';
		if(SERVER_URL.indexOf("api.parse") > -1){
			path = '/1/logout';
		}else{
			path = '/parse/logout';
		}

        var options = {
            host: urlPath,
            path: path,
            method: 'POST',
			port: PORT,
			rejectUnauthorized: false,
            headers: {
                "X-Parse-Application-Id": APPLICATION_ID,
                "X-Parse-REST-API-Key": REST_KEY,
                "X-Parse-Session-Token": SESSION_TOKEN
            }
        };

        var req = reqHttp.request(options, function (res) {
            var body = '';
            res.on('data', function (chunk) {
                body += chunk;
            });
            res.on('end', function () {
                console.log(body);
                try {
                    var jsonObj = JSON.parse(body);
                    if (jsonObj['error'] != null) {
                        upErr++;
                        toLog(jsonObj['error'], ERROR);
                    } else {
                        upOk++;
                        toLog("Logged out!", INFO);
                        isLoggedIn = false;
                        updateConfig({});
                        if (socket != null) {
                            socket.emit('login', {login: isLoggedIn, username: USERNAME, error: jsonObj["error"],
                                                    uptime: startTime.format("MM/DD/YYYY, hh:mm:ss a"), upOk: upOk, upErr: upErr, downOk: downOk, downErr: downErr});
                        }
                        return true;
                    }
                } catch (err) {
                    toLog("Server not responding", ERROR);
                }
            });
            res.on('error', function (err) {
                upErr++;
                if((err + "").includes("ENOENT")){
                    isEnoent = true;
                    toLog("No internet connection", ERROR);
                }
                return false;
            });
        });

        // req error
        req.on('error', function (err) {
            upErr++;

            if((err + "").includes("ENOENT")){
                isEnoent = true;
                toLog("No internet connection", ERROR);
            }
            return false;
        });
        req.end();
    }
};

function sendData(name, type, value) {

    if (!isLoggedIn) {
        toLog("Please log in first", WARNING);
    } else {
        toLog("Saving Data: " + type + ", " + value + " from " + name, INFO);
        updateNodes(name, type, value);
		
        var postData = {
            "dataSort": "data",
            "idName": name,
            "dataType": type,
            "value": value + "",
            "installationId": INSTALLATION_ID,
            "sessionId" : SESSION_ID,
			"os_env": osEnv,
			"os_platform": osPlatform,
			"os_arch": osArch,
			"os_hostname": osHostname,
			"os_release": osRelease,
			"loc_ip_addr": ip,
			"loc_ip_gateway": ip_gateway,
			"mac": mac
        };
        var obj = JSON.stringify(postData);

		var reqHttp;
		var urlPath;
		if (SERVER_URL.indexOf("https") > -1) {
			reqHttp = require('https');
			urlPath = SERVER_URL.substring(8);
		} else {
			reqHttp = require('http');
			urlPath = SERVER_URL.substring(7);
        }
		var path = '';
		if(SERVER_URL.indexOf("api.parse") > -1){
			path = '/1/functions/gatewayProcess';
		}else{
			path = '/parse/functions/gatewayProcess';
		}


        var options = {
            host: urlPath,
            path: path,
            method: 'POST',
			port: PORT,
			rejectUnauthorized: false,
            headers: {
                "X-Parse-Application-Id": APPLICATION_ID,
                "X-Parse-REST-API-Key": REST_KEY,
                "X-Parse-Session-Token": SESSION_TOKEN,
                "Content-Type": "application/json"
            }
        };

        var req = reqHttp.request(options, function (res) {
            var body = '';
            res.on('data', function (chunk) {
                body += chunk;
            });
            res.on('end', function () {
                console.log(body);

                toLog("Sent data to server", INFO);
                toLog(body, DEBUG);
                upOk++;
            });
            res.on('error', function (err) {
                upErr++;
                var jsonObj = JSON.parse(err);
                toLog(jsonObj['error'], ERROR);

            });
        });

        // req error
        req.on('error', function (err) {
            upErr++;
            if((err + "").includes("ENOENT")){
                isEnoent = true;
                toLog("No internet connection", ERROR);
            }
        });
        req.write(obj);
        req.end();

        /*
         var pathdir = SERVER_URL === ' ' ? '/1' : '/parse';
         var request = require('request');
         request({
         method: 'POST',
         url: SERVER_URL + pathdir + '/functions/gatewayProcess',
         body: obj,
         agentOptions: {
         secureProtocol: 'SSLv3_method'
         },
         headers: {
         "X-Parse-Application-Id": APPLICATION_ID,
         "X-Parse-REST-API-Key": REST_KEY,
         "X-Parse-Session-Token": SESSION_TOKEN,
         "Content-Type": "application/json"
         }
         }, function (error, response, body) {
         console.log(body);
         try {
         var jsonObj = JSON.parse(body);
         if (jsonObj['error'] != null) {
         toLog(jsonObj['error'], ERROR);
         } else {
         toLog("Sent data to server", INFO);
         //toLog(response.statusCode, body);
         }
         } catch (err) {
         toLog(error, ERROR);
         toLog(response.toString(), ERROR);
         toLog(body, ERROR);
         toLog("Server not responding", ERROR);
         }
         });
         */
    }

}

function sendNeighborTable(tagName, name, rssi) {

    if (!isLoggedIn) {
        toLog("Please log in first", WARNING);
    } else {
        toLog("Saving NeighborTable: "  + rssi + " from " + name, INFO);
        //updateNodes(name, addr, rssi);

        var postData = {
            "dataSort": "emNeighborTable",
            "idName": tagName,
            "nameNt": name,
            "rssiNt": rssi,
            "installationId": INSTALLATION_ID,
            "sessionId" : SESSION_ID
        };
        var obj = JSON.stringify(postData);

		var reqHttp;
		var urlPath;
		if (SERVER_URL.indexOf("https") > -1) {
			reqHttp = require('https');
			urlPath = SERVER_URL.substring(8);
		} else {
			reqHttp = require('http');
			urlPath = SERVER_URL.substring(7);
        }
		var path = '';
		if(SERVER_URL.indexOf("api.parse") > -1){
			path = '/1/functions/gatewayProcess';
		}else{
			path = '/parse/functions/gatewayProcess';
		}

        var options = {
            host: urlPath,
            path: path,
            method: 'POST',
			port: PORT,
			rejectUnauthorized: false,
            headers: {
                "X-Parse-Application-Id": APPLICATION_ID,
                "X-Parse-REST-API-Key": REST_KEY,
                "X-Parse-Session-Token": SESSION_TOKEN,
                "Content-Type": "application/json"
            }
        };

        var req = reqHttp.request(options, function (res) {
            var body = '';
            res.on('data', function (chunk) {
                body += chunk;
            });
            res.on('end', function () {
                console.log(body);
                upOk++;
                toLog("Sent data to server", INFO);
                toLog(body);
            });
            res.on('error', function (err) {
                var jsonObj = JSON.parse(err);
                toLog(jsonObj['error'], ERROR);

            });
        });

        // req error
        req.on('error', function (err) {
            upErr++;
            if((err + "").includes("ENOENT")){
                isEnoent = true;
                toLog("No internet connection", ERROR);
            }
        });
        req.write(obj);
        req.end();


        /*

        var postData = {
            "dataSort": "emNeighborTable",
            "idName": tagName,
            "nameNt": name,
            "addrNt": addr,
            "rssiNt": rssi,
            "installationId": INSTALLATION_ID
        };
        var obj = JSON.stringify(postData);
        var request = require('request');
        var pathdir = SERVER_URL === 'api.parse.com:443' ? '/1' : '/parse';
        request({
            method: 'POST',
            url: SERVER_URL + pathdir + '/functions/gatewayProcess',
            body: obj,
            agentOptions: {
                secureProtocol: 'SSLv3_method'
            },
            headers: {
                "X-Parse-Application-Id": APPLICATION_ID,
                "X-Parse-REST-API-Key": REST_KEY,
                "X-Parse-Session-Token": SESSION_TOKEN,
                "Content-Type": "application/json"
            }
        }, function (error, response, body) {
            console.log(body);
            try {
                var jsonObj = JSON.parse(body);
                if (jsonObj['error'] != null) {
                    toLog(jsonObj['error'], ERROR);
                } else {
                    toLog("Sent data to server", INFO);
                    //toLog(response.statusCode, body);
                }
            } catch (err) {
                toLog("Server not responding", ERROR);
            }
        });
        */
    }

}

function parseCommand(body) {
    //toLog(body);
    /*
     {"result":{"device":"ec4e","command":"getValue","ident":"batteryValue","gateway":{"__type":"Pointer","className":"_User","objectId":"uR5DEu3vZC"},"createdAt":"2016-10-25T00:30:45.613Z","updatedAt":"2016-10-25T00:30:45.613Z","objectId":"OSFtgpen6K","__type":"Object","className":"commandRequest"}}*/

    var jsonObj = JSON.parse(body);
    if (!isEmpty(jsonObj)) {
        var result = jsonObj['result'];
        toLog(result, DEBUG);
        var command = result['command'];
        if (command == "getValue") {
            toLog("get value", DEBUG);
            var device = result['device'];
            var ident = result['ident'];
            toLog("Request Get " + ident + " from " + device, INFO);
            emGetValue(device, ident);
        } else if (command == "setValue") {
            toLog("set value", DEBUG);
            var device = result['device'];
            var ident = result['ident'];
            var value = result['value'];
            toLog("Request Set " + ident + ", " + value + " from " + device, INFO);
            emSetValue(device, ident, value);
        } else if (command == "restart") {
            toLog("Received restart command");
            rebootPi();
        }
    }
}
/////////////////////////////////////////////////
/***********************************************

 Send to Serial

 *********************************************/

 var qStr = "";
 var timerQ = null;
function addQ(str) {
	console.log(qStr);
	console.log(str);
	if(qStr.length > 0 ) {
		
		qStr = qStr + str;
	} else{
		qStr = str;
		timerQ = setInterval(sendSer, 80);
	}
}

function sendSer() {
	if(isCommActive) {
		if(qStr.length > 0) {
			var ltr = qStr[0];
			console.log(qStr[0]);
			qStr = qStr.substring(1);
			sPort.write(new Buffer(ltr), function (error) {
				if (error !== null) {
					//downErr++;
					//toLog("Error: " + error, ERROR);
					if (timerQ != null) clearInterval(timerQ);
				} else {
					
					//downOk++;
					//toLog("Command sent!", INFO);
				}
			});
			
		}else{
			if (timerQ != null) clearInterval(timerQ);
		}	
   }
}

 
 
function emGetValue(device, ident) {
    if (isCommActive) {
        var jsonGet = {
            "cmd": "dbGetValue",
            "args": [device, ident]
        };
        var getObj = JSON.stringify(jsonGet);
        toLog(getObj, DEBUG);

		addQ(getObj + '\r');
		/*
        sPort.write(new Buffer(getObj + '\r'), function (error) {
            if (error !== null) {
                downErr++;
                toLog("Error: " + error, ERROR);
            } else {
                downOk++;
                toLog("Command sent!", INFO);
            }
        });
		*/

    }
}

function emSetValue(device, ident, value) {
    if (isCommActive) {
			var val = value.replace(/\\/g, '');
			var val2 = val.replace(/\"/g, '');
        var jsonGet = {
            "cmd": "dbSetValue",
            "args": [device, ident, val2]
        };
        var getObj = JSON.stringify(jsonGet);
        toLog(getObj, DEBUG);

		addQ(getObj + '\r');
		/*
        sPort.write(new Buffer(getObj + '\r'), function (error) {
            if (error !== null) {
                downErr++;
                toLog("Error: " + error, ERROR);
            } else {
                downOk++;
                toLog("Command sent!", INFO);
            }
        });
*/
    }
}
var emSubscribe = function (device, ident) {

    toLog("Subscribing to " + ident + ": " + device, INFO);
    if (!isCommActive) {
        var jsonSub = {
            "cmd": "dbSubscribe",
            "args": [device, ident]
        };
        var subObj = JSON.stringify(jsonSub);

		addQ(subObj + '\r');
		/*
        sPort.write(new Buffer(subObj + '\r'), function (error) {
            if (error !== null) {
                return false;
            } else {
                return "Subscribed to " + ident + ": " + device;
            }
        });
*/
    }
};
/////////////////////////////////////////////////

/***********************************************

 Scheduler

 *********************************************/

//Check if online
var schedEveryMinuteOn5 = new schedule.RecurrenceRule();
schedEveryMinuteOn5.second = 5;

var testSend = schedule.scheduleJob(schedEveryMinuteOn5, function () {

    if (!isLoggedIn) {
        toLog("Attempting to Log in...", INFO);
        getConfig();
    }

});

//Health Indicator
/*
if(osEnv == 'Linux'){
    var schedEveryMinuteOn1 = new schedule.RecurrenceRule();
    schedEveryMinuteOn1.second = 1;

    var testSend = schedule.scheduleJob(schedEveryMinuteOn1, function () {


        var tickOk = upOk + downOk;
        var tickErr = upErr + downErr;
        var tickAll = tickOk + tickErr;

        if(tickOk / tickAll > 0.90){
            //blinkGreen();
        }else if(tickOk / tickAll < 0.90 && tickOk / tickAll > 0.75){
            //blinkGreen();
            //blinkYellow();
        }else if(tickOk / tickAll < 0.75){
            //blinkYellow();
        }

    });
}
*/
var schedEveryMinOn10 = new schedule.RecurrenceRule();
schedEveryMinOn10.second = 10;   //Run every minute on 10th second

var queryCommands = schedule.scheduleJob(schedEveryMinOn10, function () {

    if (isLoggedIn && isCommActive) {

        toLog("Send data", DEBUG);
		//toLog(SESSION_ID, DEBUG);
		var reqHttp;
		var urlPath;

		if (SERVER_URL.indexOf("https") > -1) {
			reqHttp = require('https');
			urlPath = SERVER_URL.substring(8);
			console.log("HTTPS");
		} else if (SERVER_URL.indexOf("localhost") > -1){
			reqHttp = require('http');
			urlPath = 'localhost';
			console.log("HTTP");
        } else {
			reqHttp = require('http');
			urlPath = SERVER_URL.substring(7);
			console.log("HTTP");
        }
		var path = '';
		if(SERVER_URL.indexOf("api.parse") > -1){
			path = '/1/functions/getCommand';
		}else{
			path = '/parse/functions/getCommand';
		}

        var postData = {
            "sessionId": SESSION_ID
        };
        var obj = JSON.stringify(postData);

        var options = {
            host: urlPath,
            path: path,
            method: 'POST',
			port: PORT,
            headers: {
                "X-Parse-Application-Id": APPLICATION_ID,
                "X-Parse-REST-API-Key": REST_KEY,
                "X-Parse-Session-Token": SESSION_TOKEN,
                "Content-Type": "application/json"
            }
        };

        var req = reqHttp.request(options, function (res) {
            var bodyComm ='';
            res.on('data', function (chunk) {
                bodyComm += chunk;
            });
            res.on('end', function () {
				console.log(bodyComm);
				try{
					var jsonObj = JSON.parse(bodyComm);
				}catch (err){
                    downErr++;
                    //var errCode = jsonObj['err'];
                    //console.log(errCode);
				}
                console.log(jsonObj['result']);
                if(typeof jsonObj['result']['command'] != undefined && jsonObj['result']['command'] != null){
                    toLog("Found Command", INFO);
                    downOk++;
                    parseCommand(bodyComm);
                }else if(typeof jsonObj['result']['success'] != undefined && jsonObj['result']['success'] != null) {
                    downOk++;
                    toLog("No commands found");
                }else if(typeof jsonObj['result']['error'] != undefined && jsonObj['result']['error'] != null) {
                    downErr++;
                    var errCode = jsonObj['error'];
                    console.log(errCode);
                }

            });
            res.on('error', function (err) {
                downErr++;
                //var jsonErr = JSON.parse(err);

            });
        });

        // req error
        req.on('error', function (err) {
            downErr++;
            if((err + "").includes("ENOENT")){
                isEnoent = true;
                toLog("No internet connection", ERROR);
            }
        });
        req.write(obj);
        req.end();

/*
        var postData = {};
        var obj = JSON.stringify(postData);
        var request = require('request');
        var pathdir = SERVER_URL === 'api.parse.com:443' ? '/1' : '/parse';
        request({
            method: 'POST',
            url: SERVER_URL + pathdir + '/functions/getCommand',
            body: obj,
            headers: {
                "X-Parse-Application-Id": APPLICATION_ID,
                "X-Parse-REST-API-Key": REST_KEY,
                "X-Parse-Session-Token": SESSION_TOKEN,
                "Content-Type": "application/json"
            }
        }, function (error, response, body) {
            console.log(body);
            try {
                var jsonObj = JSON.parse(body);
                if (jsonObj['error'] != null) {
                    toLog(jsonObj['error'], ERROR);
                } else {
                    //toLog("Check commands");
                    toLog(response.statusCode + ", " + body, DEBUG);
                    parseCommand(body);
                }
            } catch (err) {
                toLog("No response from server", ERROR);
            }

        });
        */
    }

});


/*
 //TEST SAVE
 var schedEveryMinOn30 = new schedule.RecurrenceRule();
 schedEveryMinOn30.second = 30;   //Run every minute on 10th second

 var testSend = schedule.scheduleJob(schedEveryMinOn30, function () {

 if (isLoggedIn) {

 }

 });
 */


/////////////////////////////////////////////////
/***********************************************

 SOCKET.IO

 *********************************************/

io.on('connection', function (sockt) {
    toLog('User connected', INFO);
    isSocketOn = true;
    socket = sockt;
    //toLog(isLoggedIn);
    socket.emit('login', {
        login: isLoggedIn,
        username: USERNAME,
        serverurl: SERVER_URL,
        domainname: DOMAIN_NAME,
        domaintzone: DOMAIN_TZONE,
        uptime: startTime.format("MM/DD/YYYY, hh:mm:ss a"), upOk: upOk, upErr: upErr, downOk: downOk, downErr: downErr}
    );
    updateUsb(null, null, null);



    //socket.broadcast.emit('event', "Welcome user");
    socket.on('disconnect', function () {
        toLog('User disconnected', INFO);
        isSocketOn = false;
    });

    socket.on('mesh', function (mesh) {
        if (mesh.reset) {
            resetUsb();
        }
    });

    socket.on('reboot', function (res) {
        if (res) {
            rebootPi();
        }
    });

    socket.on('wifi', function (mesh) {
        toLog("Scan WiFi");
        //  Try scanning for access points:
        WiFiControl.scanForWiFi(function (err, response) {
            if (err) {

            } else {
                var networks = [];
                for (var i = 0; i < response['networks'].length; i++)
                    networks.push(response['networks'][i]['ssid']);
                console.log(networks);
                socket.emit('wifi', networks);
            }
        });
    });
    socket.on('change_wifi', function (res) {
        console.log(res);

        var results = WiFiControl.connectToAP(res, function (err, response) {
            if (err) {
                socket.emit('change_wifi', err);
                toLog(err, ERROR);
                console.log(err);
            } else {
                socket.emit('change_wifi', "Network Change Successful!");
                toLog("Network Changed", INFO);
                updateIp();
            }
        });

    });
    updateIp();
    socket.on('login', function (res) {
		console.log(res);
        if (res != null) {
            if (!res.login) {
                logout();
            } else {
                login(res.username, res.password, res.serverurl);
            }
        }
    });

    socket.on('command', function (cmd) {
        if (cmd.run) {
            toLog("Opening port", INFO);
            getPort();
            //runGateway(socket);
        } else if (!cmd.run) {
            toLog("Closing port", INFO);
            closePort();
        }
    });
});

function ioUpdateNodes() {
    if (socket != null) {
        socket.emit('nodes', nodeObj);
    }
}

http.listen(80, function () {
    toLog('listening on *:' + 80, INFO);
});
/////////////////////////////////////////////////
/***********************************************

 USB

 *********************************************/
function updateUsb(attached, connect, comment) {
    if (attached !== null) {
        isUsbAttached = attached;
    }
    if (connect !== null) {
        isCommActive = connect;
    }

    if (comment != null)
        usbStatus = comment;

    if (socket != null) {
        socket.emit('isconnect', {attached: isUsbAttached, usb_connect: isCommActive, comment: usbStatus});
    }
}

function resetUsb() {
    closePort();
}

usb.on('attach', function (device) {
    toLog("USB device attached", INFO);
    if (!isCommActive) {
        getPort();
    }
});

usb.on('detach', function (device) {
    toLog("USB device detached", ERROR);
    updateUsb(false, false, "USB Station detached");
});

function getPort() {
    updateUsb(false, false, "USB Station not connected");
    toLog("Getting list of ports...", INFO);
    serialport.list(function (err, ports) {
        toLog(JSON.stringify(ports), DEBUG);
        ports.forEach(function (port) {
            if (osEnv === "Windows_NT") {
                if (port.pnpId === 'USB\\VID_0451&PID_16AA\\030') {
                    updateUsb(true, false, "USB Station attached, initializing...");
                    isUsbAttached = true;
                    toLog("Found Station Device", INFO);
                    USB_PORT_NAME = port.comName;
                    connectPort(port.comName);
                } else {
                    //toLog("No USB Device found!");
                }
            } else if (osEnv === "Linux") {
                if (port.vendorId === '0x0451' && port.productId === '0x16aa') {
                    updateUsb(true, false, "USB Station attached, initializing...");
                    isUsbAttached = true;
                    toLog("Found Station Device", INFO);
                    USB_PORT_NAME = port.comName;
                    connectPort(port.comName);
                } else {
                    //toLog("No USB Device found!");
                }

            }
        });
    });
}

function connectPort(portName) {

    sPort = new serialport(portName, {
        baudRate: 921600,
	dataBits: 8,		//5,6,7,8
	stopBits: 1,		//1,2
	parity: 'none',		//none, even, mark,odd,space
	rtscts: true,
	xon: false,
	xoff: false,
	xany: false,
	bufferSize: 65536,
        // look for return and newline at the end of each data packet:
        parser: serialport.parsers.readline('\n')
    });

    sPort.on('open', function () {
		updateUsb(true, true, "OK");
		isCommActive = true;
		toLog("Serial communication is active!", INFO);
        //subscribeIdents(identSubs, identSubs.length);
        toLog('Port open. Data rate: ' + sPort.options.baudRate, INFO);
        sPort.flush(function () {
			//addQ('[mcusb]' + '\r');
			/*
            sPort.write(new Buffer('\r' + '[mcusb]' + '\r'), function (error) {
                if (error != null)
                    toLog("Error: " + error, ERROR);
            });
			*/
            sPort.on('data', function (data) {
                sendSerialData(data);
            });
        });

    });
}

function closePort() {

    if (isCommActive) {
        sPort.close(function (error) {
            updateUsb(true, false, "USB Port closed");
            isCommActive = false;
            sPort = null;
            getPort();
        });
    }
}

function subscribeIdents(identSubs, i) {
    setTimeout(function () {
        emSubscribe("*", identSubs[i - 1]);
        // DO SOMETHING WITH data AND stuff
        if (--i) {                  // If i > 0, keep going
            subscribeIdents(identSubs, i);  // Call the loop again
        } else {
            updateUsb(true, true, "OK");
            isCommActive = true;
            toLog("Serial communication is active!", INFO);
        }
    }, 3000);
}


function sendSerialData(data) {
    toLog('Data: ' + data, DEBUG);
    //socket.emit('event', data);
    if (data.indexOf("[USB]") > -1) {
        toLog("Subscribing to all data objects", INFO);

    } else if (data.indexOf("group") > -1) {
        toLog("Data Present", INFO);
		var dataStr = data.substring(data.indexOf('{"group'), data.indexOf('}') + 1);
		var fixStr = dataStr.replace('[', '"[');
		var fixStr2 = fixStr.replace(']', ']"');
		console.log('Data Object: ' + fixStr2);
		try{
        var jsonObj = JSON.parse(fixStr2);
		}catch (err) {
			return;
		}
        //console.log(jsonObj['group']);
        //console.log(Object.keys(jsonObj)[1]);
        sendData(jsonObj['group'], Object.keys(jsonObj)[1], jsonObj[Object.keys(jsonObj)[1]]);

    } else if (data.indexOf("emNeighborTable") > -1) {
        toLog("NeighborTable Present", INFO);

		var count = (data.match(/"addr":/g) || []).length;
		console.log("Count: " + count);
		var lastPos = data.lastIndexOf('"addr":');
		console.log("Last Position: " + lastPos);
		var stBrInd = data.indexOf("[");
		var endBrInd = data.indexOf("]");
		var ntArr = data.substr(stBrInd, endBrInd - 2);
		var guess = '"addr":';
		var newStr = "";
		var counter = 0;

		for(var i = 0; i < count; i++){
			var subStr = data.substring(data.indexOf('"addr":'));
			var subStr2 = subStr.substring(0, subStr.indexOf(',')+ 1);
		  console.log(subStr);
		  var addrStr = '';
		  newStr = data.replace(subStr2, addrStr);
		  data = newStr;
		  if(i == count - 1){
		  console.log(data);
			try {
				jsonObj = JSON.parse(newStr);
				var tagName = jsonObj['emNeighborTable'];
				if (jsonObj['set'] !== "[]" || jsonObj['set'] !== null) {
					console.log(jsonObj['set'][0]['name']);
					console.log(jsonObj['set'][0]['addr']);
					console.log(jsonObj['set'][0]['rssi']);
					for (var i = 0; i < jsonObj['set'].length; i++) {
						var name = jsonObj['set'][i]['name'];
						var rssi = jsonObj['set'][i]['rssi'];
						sendNeighborTable(tagName, name, rssi);

					}
				}
			} catch (err) {
				toLog(err, ERROR);
			}
		  }
		}
        /*
        var stBrInd = data.indexOf("[");
        var endBrInd = data.indexOf("]");
        var ntArr = data.substr(stBrInd, endBrInd - 2);
        toLog(ntArr, DEBUG);
        var guess = '"addr":';
        var newStr = "";
        for (var index = data.indexOf(guess);
                index >= 0;
                index = data.indexOf(guess, index + 1))
        {
            var subStr = data.substring(index + 7, index + 11);
            var addrStr = '"' + subStr + '"';
            newStr = data.replace(subStr, addrStr);
        }
         //var count = (data.match(/"addr":/g) || []).length;
        // for(var i = 0; i < count; i++) {
         //console.log(data.indexOf("addr:"));
         //var subStr = data.substr(0, data.indexOf("addr\":") + 5);
         //console.log(subStr.substr(0, 4));
         //}\
        try {
            jsonObj = JSON.parse(newStr);
            //{"emNeighborTable":"wUEL","set":[{"name":"e8I7","addr":1bc9,"rssi":-39}]}
            var tagName = jsonObj['emNeighborTable'];
            if (jsonObj['set'] !== "[]" || jsonObj['set'] !== null) {
                //console.log(jsonObj['set'][0]['name']);
                //console.log(jsonObj['set'][0]['addr']);
                //console.log(jsonObj['set'][0]['rssi']);
                for (var i = 0; i < jsonObj['set'].length; i++) {
                    var name = jsonObj['set'][i]['name'];
                    var addr = jsonObj['set'][i]['addr'];
                    var rssi = jsonObj['set'][i]['rssi'];
                    sendNeighborTable(tagName, name, addr, rssi);

                }
            }
        } catch (err) {
            toLog(err, ERROR);
        }
        */
    }
}

/////////////////////////////////////////////////
/***********************************************

 UTILS

 *********************************************/
function rebootPi () {
    if(osEnv === "Linux") {
        logout();
        //then(function (res) {
            //if(res){
                toLog("Logged out, Restarting now...", INFO);
                exec('sudo shutdown -r now', function (error, stdout, stderr) {

                });
            //}else{

            //}
       // });
    }else{
        console.log("Command use for Linux only");
    }

}


function updateNodes(name, ident, value) {

    if (nodeObj !== null) {
        if (nodeObj[name] === undefined) {

            //toLog("node doesn't exist", DEBUG);
            var identKey = ident;
            var identObj = {};
            identObj[identKey] = value;
            var identArr = [];
            identArr.push(identObj);
            var nameKey = name;
            var nameObj = {};
            //nameObj[nameKey] = identArr;
            nodeObj[nameKey] = identArr;
            //console.log(JSON.stringify(nodeObj));

        } else {
            console.log("node exists");
            //console.log(nodeObj[name]);
            var foundIdent = false;
            for (var i = 0; i < nodeObj[name].length; i++) {
                if (ident in nodeObj[name][i]) {
                    //console.log("found ident");
                    foundIdent = true;
                }
                if (i === nodeObj[name].length - 1) {
                    if (foundIdent) {
                        //console.log("ident exists");
                        var typeObj = nodeObj[name][i];
                        typeObj[ident] = value;
                        //console.log(nodeObj);
                        ioUpdateNodes();
                    } else {
                        var identKey = ident;
                        var identObj = {};
                        identObj[identKey] = value;
                        //console.log("ident doesn't exist");
                        nodeObj[name].push(identObj);
                        //console.log(JSON.stringify(nodeObj));
                        ioUpdateNodes();
                    }
                }
            }
        }
    } else {
        //console.log("node doesn't exist");
        var identKey = ident;
        var identObj = {};
        identObj[identKey] = value;
        var identArr = [];
        identArr.push(identObj);
        var nameKey = name;
        var nameObj = {};
        //nameObj[nameKey] = identArr;
        nodeObj[nameKey] = identArr;
        //console.log(JSON.stringify(nodeObj));
        ioUpdateNodes();
    }
}


function isEmpty(obj) {
    return !Object.keys(obj).length > 0;
}

process.stdin.resume();//so the program will not close instantly

function exitHandler(options, err) {
    if (options.cleanup) {
        toLog('Gateway Closing', WARNING);
    }
    ;
    if (err){
		if(err.code == "EADDRINUSE") return;
        toLog(err.stack, ERROR);
	}
    if (options.exit) {
	//bleno.disconnect();
    logout();
    rebootPi();
    process.exit();
    }
    ;
}

//do something when app is closing
process.on('exit', exitHandler.bind(null, {cleanup: true}));

//catches ctrl+c event
process.on('SIGINT', exitHandler.bind(null, {exit: true}));

//catches uncaught exceptions
process.on('uncaughtException', exitHandler.bind(null, {exit: true}));
/////////////////////////////////////////////////
/***********************************************

 Log Handling

 *********************************************/

var DEBUG = "debug";
var INFO = "info";
var WARNING = "warning";
var ERROR = "error";

function toLog(string, type) {

    var colors = require('colors');

    var time = moment();
	var logPhrase = "(" + type + ")" + moment().format("MM/DD/YY, HH:mm:ss") + ": " + string;
	
	if (enableLogging) appendToLog(logPhrase);
	
    if (type === DEBUG) {
        console.log(logPhrase.blue);
        if (isSocketOn && socket != null) {
            socket.emit('info', logPhrase);
        }
    } else if (type === INFO) {
        console.log(logPhrase.green);
        if (isSocketOn && socket != null) {
            socket.emit('info', logPhrase);
        }
    } else if (type === WARNING) {
        console.log(logPhrase.yellow);
    } else if (type === ERROR) {
        if (isSocketOn && socket != null) {
            socket.emit('info', logPhrase);
        }
        console.log(logPhrase.red);
    } else {
        console.log(logPhrase);
    }

}
